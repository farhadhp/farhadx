---
layout: page
title: درباره من
permalink: /about/
---
> تا الان بیش از 10ها بار متن درباره خودم رو نوشتم و بعد از مدتی حذف کردم دوباره از اول نوشتم. متنی که در زیر می‌خوانید یکی از این 10ها نوشته‌هاست. و در آینده ممکنه دوباره از نو بنویسمش. نوشتن درباره خود جزوه یکی از سخت‌ترین کارهای دنیاست و هر چقدر هم که بنویسی باز نمی‌تونی اونی که هستی رو دقیق معرفی کنی.

**فرهاد** هستم!؛ برنامه نویسی حرفه‌ای رو از اواخر سال ۸۹ شمسی شروع کردم؛ با اکثر زبانهای برنامه نویسی مطرح آشنایی دارم؛ با زبان‌ c برای میکروکنترولرها و با زبان php سمت سرور و هم چنین با html و css و جاوا اسکریپت (همراه با کتابخونه و فریم‌ورکاش)‌ سمت کاربر بیشتر از هر چیزی کدنویسی کردم.

با اکثر فریم ورک‌های php آشنایی دارم؛ ولی فریم ورکی که بیشتر با آن وب اپلیکیشن و … ساختم کدایگنایتر هست. با php از ساخت اسکریپت تا ربات تلگرامی و اسکریپت‌های تحت کامند لاین و وب سرویس و نرم افزارهای تحت دسکتاپ برنامه نویسی کردم (البته به عقیده خودم بازدهی php سمت سرور به مراتب بهتر و بیشتر از تحت دسکتاپ هست!).

همیشه دنبال یادگیری هستم و سعی میکنم همیشه خودم رو بروز نگهدارم؛ این اواخر هم علاقه خاصی به زبان برنامه نویسی روبی و فریم ورک محبوبش (ریلز) پیدا کردم و در اوقات بیکاری مقالاتی در رابطه باهاش میخونم.

به **طبیعت **و **کیهان شناسی** هم علاقه دارم و بیشتر مستنداتی در این رابطه می‌بینم؛ از فیلم‌های سینمایی در اوقات فراغت غافل نمیشم و بیشتر فیلم‌های علمی تخیلی و اکشن خارجی رو دنبال می‌کنم.

از بین مدیریت محتواهای رایج وردپرس رو بدلیل پیچیدگی کمتر و سادگی دوست دارم؛ برای وردپرس طراحی قالب و افزونه نویسی هم انجام میدم.

همیشه تکنولوژی، پلتفورم و فریمورک‌های جدید رو امتحان می‌کنم!

> به نرم افزار آزاد اعتقاد دارم و همیشه سعی میکنم از سیستم عامل تا نرم افزاری که استفاده میکنم آزاد باشه (آزاد نه به معنای رایگان!!!).

از بین شبکه‌های اجتماعی موجود بیشتر از همه در گیت‌هاب (1) و توییتر فعالیت می‌کنم.

وبلاگ نویسی هم یکی از علایق دیرینه منه و تا الان که این متن رو مینویسم صاحب بیش از ۱۰ وبلاگ و چندین وب سایت **بودم** :-).

[توییتر](https://twitter.com/FarhadxFarhad)

[مشاهده رزومه](http://farhadcv.ir/)

[گیت‌هاب](https://github.com/farhadhp)

### پانوشت

1: گیت‌هاب درسته شبکه اجتماعی نیست اما میشه گفت یه جورایی شبکه اجتماعی برنامه نویس‌هاست که بجای تصویر سورس کد پروژه‌هاشون رو به اشتراک می‌گذارند.